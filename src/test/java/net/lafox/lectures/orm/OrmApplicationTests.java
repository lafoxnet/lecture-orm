package net.lafox.lectures.orm;

import net.lafox.lectures.orm.entity.*;
import net.lafox.lectures.orm.repository.PassportRepository;
import net.lafox.lectures.orm.repository.StudentRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashSet;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
public class OrmApplicationTests {
    @Autowired
    private PassportRepository passportRepository;

    @Autowired
    private StudentRepository  studentRepository;
    @Test
    public void createStudent(){
        Passport passport = new Passport();
        passport.passportNumber = "SK123456";

        Course course = new Course();
        course.name="Math";

        Address address = new Address();
        address.address="New York";

        Phone phone1 = new Phone();
        phone1.phoneNumber="123456";

        Phone phone2 = new Phone();
        phone2.phoneNumber="555555";

        Student student= new Student();
        student.address=address;
        student.passport=passport;

        student.studentPhones= new HashSet<>();
        student.studentPhones.add(phone1);
        student.studentPhones.add(phone2);

        student.studentCourses = new HashSet<>();
        student.studentCourses.add(course);

        studentRepository.save(student);


    }


    @Test
    public void createPassport() {
        Passport passport = new Passport();
        passport.passportNumber = "SK123456";

        passportRepository.save(passport);

        Passport expected = new Passport();
        expected.passportNumber = "SK123456";

        assertEquals(expected.passportNumber, passport.passportNumber);
    }

}
