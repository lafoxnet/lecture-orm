package net.lafox.lectures.orm.entity;

import javax.persistence.*;
import java.util.Set;

@Entity
public class Student {
    @Id
    @GeneratedValue
    public Integer id;
    public String name;

    @OneToOne(cascade = CascadeType.ALL)
    public Passport passport;

    @OneToMany(cascade = CascadeType.ALL)
    public Set<Phone> studentPhones;

    @ManyToOne(cascade = CascadeType.ALL)
    public Address address;

    @ManyToMany(cascade = CascadeType.ALL)
    public Set<Course> studentCourses;
}
