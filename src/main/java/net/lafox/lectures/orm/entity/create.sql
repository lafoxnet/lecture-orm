-- drop table student_student_phones cascade ;
-- drop table phone cascade;
-- drop table student_student_courses cascade;
-- drop table student cascade;
-- drop table passport cascade;
-- drop table address cascade;
-- drop table course cascade;
-- drop sequence hibernate_sequence;

create sequence hibernate_sequence;

create table passport (
  id int primary key,
  passport_number text
);

create table address (
  id      int primary key,
  address text
);

CREATE TABLE student (
id   int primary key,
name text,
passport_id int references passport(id),
address_id int references address(id)

);



create table phone(
  id int primary key,
  phone_number text
);

create table student_student_phones(
  student_id int references student(id),
  student_phones_id int references phone(id),
  primary key (student_id, student_phones_id)
);

create table course (
  id   int primary key,
  name text
);

create table student_student_courses(
  student_id int references student(id),
  student_courses_id int references course(id),
  primary key (student_id, student_courses_id)
);


