package net.lafox.lectures.orm.repository;

import net.lafox.lectures.orm.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudentRepository extends JpaRepository<Student, Integer> {
}
