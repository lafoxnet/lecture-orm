package net.lafox.lectures.orm.repository;

import net.lafox.lectures.orm.entity.Passport;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

@Component
public interface PassportRepository extends JpaRepository<Passport, Integer> {
}
